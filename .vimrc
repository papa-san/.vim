" pathogen
execute pathogen#infect()


"set nocompatible                " choose no compatibility with legacy vi
syntax enable
"set encoding=utf-8
set showcmd                     " display incomplete commands
"filetype plugin indent on       " load file type plugins + indentation
set ruler                       " show line and column number of the cursor position

"" Whitespace
"set nowrap                      " don't wrap lines
"set wrapmargin=1
"set tabstop=4 shiftwidth=4      " a tab is x spaces 
"set expandtab                   " use spaces, not tabs (optional)
"set backspace=indent,eol,start  " backspace through everything in insert mode
let c_space_errors = 1          " show trailing spaces in c files

"" Searching
"set hlsearch                    " highlight matches
"set incsearch                   " incremental searching
set ignorecase                  " searches are case insensitive...
set smartcase                   " ... unless they contain at least one capital letter

"" Identation and helpers
"set autoindent
set showmatch                   " show match of brackets
"set scrolloff=2                 " keep x lines visible above and below the cursor

"Use ctags: ctags -R ."
